# matfHub

matfHub je projekat namenjen studentima Matematičkog fakulteta koji u sebi sadrzi alate za jednostavnu organizaciju, praćenje nastave i pripremu ispita.

- Članovi:
    - <a href="https://gitlab.com/lmila">Mila Lukic 222/2018</a>
    - <a href="https://gitlab.com/Spera97">Stevan Popovic 352/2020</a>
    - <a href="https://gitlab.com/EUFrosty">Bojan Veličković 373/2020</a>
    - <a href="https://gitlab.com/DzzzD">David Zivkovic 98/2020</a>
    - <a href="https://gitlab.com/matija0201">Matija Stankovic 171/2020</a>
    - <a href="https://gitlab.com/6ananaana">Ana Vucinic 200/2018</a>


- Uputstvo za gradjenje projekta:
   - pokrenuti `./prebuild.sh`
   - otovriti repo u QtCreatoru
   - pritisnuti build dugme

- <a href="https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/matfHub/-/wikis/home">Wiki</a>
